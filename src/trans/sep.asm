global sepia_bundle_sse

section .text

; rdi - source's pixels
; rsi - target's pixels
; rdx - pixel count
sepia_bundle_sse:
    test rdx, rdx
    jnz .begin
    ret

    .begin:
        push r12
        push r13
        mov rcx, 0xFF000000

    .loop:
        ; ============================
        ; xmm0 - source reds
        ; xmm1 - source greens
        ; xmm2 - source blues
        ;
        ; xmm6 - temp or buffer
        ; xmm7 - temp or buffer
        ; 
        ; xmm3 - target reds
        ; xmm4 - target greens
        ; xmm5 - target blues
        ; ============================

        ; ----------------------------
        ; unpaking pixels from source
        ; ----------------------------

        ; upacking r1|g1|b1|r2
        mov eax, [rdi+0]
        mov r8, rax
        mov r9, rax
        mov r10, rax
        shr r9, 8
        shr r10, 16
        shr rax, 16
        and r8, 0x000000FF
        and r9, 0x000000FF
        and r10, 0x000000FF
        and rax, 0x0000FF00
        or r8, rax

        ; upacking g2|b2|r3|g3
        mov eax, [rdi+4]
        mov r11, rax
        mov r12, rax
        mov r13, rax
        shl r11, 8
        shr rax, 8
        and r11, 0x0000FF00
        and r12, 0x0000FF00
        and r13, 0x00FF0000
        and rax, 0x00FF0000
        or r9, r11
        or r10, r12
        or r8, r13
        or r9, rax

        ; upacking b3|r4|g4|b4
        mov eax, [rdi+8]
        mov r11, rax
        mov r12, rax
        mov r13, rax
        shl r11, 16
        shl r12, 16
        shl r13, 8
        and r11, 0x00FF0000
        and r12, rcx ; 0xFF000000
        and r13, rcx ; 0xFF000000
        and rax, rcx ; 0xFF000000
        or r10, r11
        or r8, r12
        or r9, r13
        or r10, rax

        movq xmm0, r8
        movq xmm1, r9
        movq xmm2, r10
        pxor xmm6, xmm6

        punpcklbw xmm0, xmm6  ; 0|0|0|0|0|0|0|0|0|r1|0|r2|0|r3|0|r4
        punpcklbw xmm1, xmm6
        punpcklbw xmm2, xmm6

        punpcklwd xmm0, xmm6  ; 0|0|0|r1|0|0|0|r2|0|0|0|r3|0|0|0|r4
        punpcklwd xmm1, xmm6
        punpcklwd xmm2, xmm6

        cvtdq2ps xmm0, xmm0
        cvtdq2ps xmm1, xmm1
        cvtdq2ps xmm2, xmm2

        ; ------------------------
        ; applying the conversion
        ; ------------------------

        ; 8 7 6 from [cc]
        mov r11d, 1049314198  ; pre-calculated
        mov r12d, 1056964608  ; pre-calculated
        mov r13d, 1040590045  ; pre-calculated

        movq xmm3, r11
        movq xmm6, r12
        movq xmm7, r13

        punpckldq xmm3, xmm3  ; 0|c11|0|c11
        punpckldq xmm6, xmm6
        punpckldq xmm7, xmm7

        punpcklqdq xmm3, xmm3  ; c11|c11|c11|c11
        punpcklqdq xmm6, xmm6
        punpcklqdq xmm7, xmm7

        mulps xmm3, xmm0  ; (c11 * r[i]) i in [1...4]
        mulps xmm6, xmm1  ; (c12 * g[i]) i in [1...4]
        mulps xmm7, xmm2  ; (c13 * b[i]) i in [1...4]

        addps xmm3, xmm6  ; (c11 * r[i] + c12 * g[i]) i in [1...4]
        addps xmm3, xmm7  ; (c11 * r[i] + c12 * g[i] + c13 * b[i]) == result(r[i]) i in [1...4]

        ; 5 4 3 from [cc]
        mov r11d, 1051025474  ; pre-calculated
        mov r12d, 1060085170  ; pre-calculated
        mov r13d, 1043073073  ; pre-calculated

        movq xmm4, r11
        movq xmm6, r12
        movq xmm7, r13

        punpckldq xmm4, xmm4
        punpckldq xmm6, xmm6
        punpckldq xmm7, xmm7

        punpcklqdq xmm4, xmm4
        punpcklqdq xmm6, xmm6
        punpcklqdq xmm7, xmm7

        mulps xmm4, xmm0
        mulps xmm6, xmm1
        mulps xmm7, xmm2

        addps xmm4, xmm6
        addps xmm4, xmm7  ; result(g[i]) i in [1...4]

        ; 2 1 0 from [cc]
        mov r11d, 1053374284  ; pre-calculated
        mov r12d, 1061226021  ; pre-calculated
        mov r13d, 1045287666  ; pre-calculated

        movq xmm5, r11
        movq xmm6, r12
        movq xmm7, r13

        punpckldq xmm5, xmm5
        punpckldq xmm6, xmm6
        punpckldq xmm7, xmm7

        punpcklqdq xmm5, xmm5
        punpcklqdq xmm6, xmm6
        punpcklqdq xmm7, xmm7

        mulps xmm5, xmm0
        mulps xmm6, xmm1
        mulps xmm7, xmm2

        addps xmm5, xmm6
        addps xmm5, xmm7  ; result(b[i]) i in [1...4]

        ; --------------------------
        ; paking pixels into target
        ; --------------------------

        ; converting results
        cvtps2dq xmm3, xmm3
        cvtps2dq xmm4, xmm4
        cvtps2dq xmm5, xmm5

        packssdw xmm3, xmm3
        packusdw xmm4, xmm4
        packusdw xmm5, xmm5

        packuswb xmm3, xmm3
        packuswb xmm4, xmm4
        packuswb xmm5, xmm5

        movq r8, xmm3   ; sat(result(r[i])) i in [1...4]
        movq r9, xmm4   ; sat(result(g[i])) i in [1...4]
        movq r10, xmm5  ; sat(result(b[i])) i in [1...4]

        ; packing r1|g1|b1|r2
        mov r11, r8
        mov r12, r9
        mov r13, r10
        mov rax, r8
        and r11, 0x000000FF
        and r12, 0x000000FF
        and r13, 0x000000FF
        and rax, 0x0000FF00
        shl r12, 8
        shl r13, 16
        shl rax, 16
        or r12, r13
        or rax, r11
        or rax, r12
        mov [rsi+0], eax

        ; packing g2|b2|r3|g3
        mov r11, r9
        mov r12, r10
        mov r13, r8
        mov rax, r9
        and r11, 0x0000FF00
        and r12, 0x0000FF00
        and r13, 0x00FF0000
        and rax, 0x00FF0000
        shr r11, 8
        shl rax, 8
        or r12, r13
        or rax, r11
        or rax, r12
        mov [rsi+4], eax

        ; packing b3|r4|g4|b4
        mov r11, r10
        mov r12, r8
        mov r13, r9
        mov rax, r10
        and r11, 0x00FF0000
        and r12, rcx ; 0xFF000000
        and r13, rcx ; 0xFF000000
        and rax, rcx ; 0xFF000000
        shr r11, 16
        shr r12, 16
        shr r13, 8
        or r12, r13
        or rax, r11
        or rax, r12
        mov [rsi+8], eax

        ; move pointers & decrease the counter
        add rdi, 12
        add rsi, 12
        dec rdx
        jnz .loop

    pop r13
    pop r12
    ret
