#ifndef SEPIA_H
#define SEPIA_H

#include "../image/image.h"

struct image sepia_image_native(struct image source);

struct image sepia_image_sse(struct image source);

#endif