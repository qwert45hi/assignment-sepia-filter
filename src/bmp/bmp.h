#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

#include "../image/image.h"

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static const uint32_t TYPE = 19778;
static const uint32_t RESERVED = 0;
static const uint32_t SIZE = 40;
static const uint16_t PLANES = 1;
static const uint16_t BIT_COUNT = 24;
static const uint32_t COMPRESSION = 0;
static const uint32_t PIXELS_PER_METER = 2834;
static const uint32_t CLR_USED = 0;
static const uint32_t CLR_IMPORTANT = 0;

enum read_status { READ_OK = 0, READ_INVALID_HEADER, READ_ERROR };

enum read_status from_bmp(FILE* in, struct image* img);

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_SOURCE,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, struct image* img);

#endif
