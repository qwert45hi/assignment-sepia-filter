#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include "bmp/bmp.h"
#include "file/file.h"
#include "image/image.h"
#include "trans/sep.h"

void timed(char* prompt, struct image image, struct image (*f)(struct image)) {
    static const int64_t repeats = 1000;
    struct rusage r;

    getrusage(RUSAGE_SELF, &r);
    struct timeval temp = r.ru_utime;

    struct image new_image;
    for (uint64_t i = 0; i < repeats; i++) new_image = f(image);
    image_destroy(&new_image);

    getrusage(RUSAGE_SELF, &r);
    printf("Time elapsed for %" PRIu64 " repeats of %s: %ld (ms)\n", repeats, prompt,
           ((r.ru_utime.tv_sec - temp.tv_sec) * 1000000L) + r.ru_utime.tv_usec - temp.tv_usec);
}

int main(void) {
    char* input_file_name = "files/input.bmp";

    FILE* fin = NULL;
    switch (open_file(&fin, input_file_name, "rb")) {
        case IO_NO_SUCH_FILE_OR_DIRECTORY:
            fprintf(stderr, "No such file or directory: %s\n", input_file_name);
            return 1;
        case IO_EMPTY_TARGET:
            fprintf(stderr, "Input file pointer broke\n");
            return 1;
        case IO_OK:
            fprintf(stdin, "Input file opened\n");
            break;
    }

    struct image image = {0};
    bool error = true;
    switch (from_bmp(fin, &image)) {
        case READ_OK:
            fprintf(stdin, "Image loaded successfully\n");
            error = false;
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "File has an invalid header\n");
            break;
        case READ_ERROR:
            fprintf(stderr, "Read error appeared\n");
            break;
    }

    if (!error) {
        timed("wholly-native C conversion", image, sepia_image_native);
        timed("SSE-accelerated conversion", image, sepia_image_sse);
    }

    image_destroy(&image);
    close_file(&fin);

    return error ? 1 : 0;
}