#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct pixel {
    uint8_t blue, green, red;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t height, uint64_t width);

struct pixel image_get_pixel(struct image* image, uint64_t x, uint64_t y);

void image_set_pixel(struct image* image, struct pixel pixel, uint64_t x, uint64_t y);

void image_destroy(struct image* image);

#endif
