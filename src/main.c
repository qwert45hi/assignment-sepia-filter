#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

#include "bmp/bmp.h"
#include "file/file.h"
#include "image/image.h"
#include "trans/sep.h"

int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr,
                "Use this program with <input_file_name>, <output_file_name_native> "
                "and <output_file_name_sse> arguments\n");
        return 1;
    }
    char* input_file_name = argv[1];
    char* output_file_name_native = argv[2];
    char* output_file_name_sse = argv[3];

    FILE* fin = NULL;
    switch (open_file(&fin, input_file_name, "rb")) {
        case IO_NO_SUCH_FILE_OR_DIRECTORY:
            fprintf(stderr, "No such file or directory: %s\n", input_file_name);
            return 1;
        case IO_EMPTY_TARGET:
            fprintf(stderr, "Input file pointer broke\n");
            return 1;
        case IO_OK:
            fprintf(stdin, "Input file opened\n");
            break;
    }

    FILE* fout_native = NULL;
    switch (open_file(&fout_native, output_file_name_native, "wb")) {
        case IO_NO_SUCH_FILE_OR_DIRECTORY:
            fprintf(stderr, "No such file or directory: %s\n", output_file_name_native);
            return 1;
        case IO_EMPTY_TARGET:
            fprintf(stderr, "Output filepointer (1) broke\n");
            return 1;
        case IO_OK:
            fprintf(stdin, "Output file (1) opened\n");
            break;
    }

    FILE* fout_sse = NULL;
    switch (open_file(&fout_sse, output_file_name_sse, "wb")) {
        case IO_NO_SUCH_FILE_OR_DIRECTORY:
            fprintf(stderr, "No such file or directory: %s\n", output_file_name_sse);
            return 1;
        case IO_EMPTY_TARGET:
            fprintf(stderr, "Output file pointer (2) broke\n");
            return 1;
        case IO_OK:
            fprintf(stdin, "Output file (2) opened\n");
            break;
    }

    struct image image = {0};
    bool error = true;
    switch (from_bmp(fin, &image)) {
        case READ_OK:
            fprintf(stdin, "Image loaded successfully\n");
            error = false;
            break;
        case READ_INVALID_HEADER:
            fprintf(stderr, "File has an invalid header\n");
            break;
        case READ_ERROR:
            fprintf(stderr, "Read error appeared\n");
            break;
    }

    if (!error) {
        error = true;
        struct image new_image_native = sepia_image_native(image);
        switch (to_bmp(fout_native, &new_image_native)) {
            case WRITE_OK:
                fprintf(stdin, "Natively converted image saved successfully\n");
                error = false;
                break;
            case WRITE_INVALID_SOURCE:
                fprintf(stderr, "Image broke 2\n");
                break;
            case WRITE_HEADER_ERROR:
                fprintf(stderr, "Error appeared while writing new image-native's header\n");
                break;
            case WRITE_ERROR:
                fprintf(stderr, "Error appeared while writing new image-native\n");
                break;
        }
        image_destroy(&new_image_native);
    }

    if (!error) {
        error = true;
        struct image new_image_sse = sepia_image_sse(image);
        switch (to_bmp(fout_sse, &new_image_sse)) {
            case WRITE_OK:
                fprintf(stdin, "SSE-converted image saved successfully\n");
                error = false;
                break;
            case WRITE_INVALID_SOURCE:
                fprintf(stderr, "Image broke 3\n");
                break;
            case WRITE_HEADER_ERROR:
                fprintf(stderr, "Error appeared while writing new image-sse's header\n");
                break;
            case WRITE_ERROR:
                fprintf(stderr, "Error appeared while writing new image-sse\n");
                break;
        }
        image_destroy(&new_image_sse);
    }

    image_destroy(&image);
    close_file(&fin);
    close_file(&fout_native);
    close_file(&fout_sse);

    return error ? 1 : 0;
}
