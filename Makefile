.PHONY: clean-all test

CC=gcc
ASM=nasm
CFLAGS=-std=c17 -O3
AFLAGS=-felf64

SOURCE = src
OBJECT = obj
BUILD = build

all: main timed clean

test: all
	./$(BUILD)/main files/input.bmp files/output-native.bmp files/output-sse.bmp
	./$(BUILD)/timed
	
timed: mkdir c-all asm-all
	$(CC) $(CFLAGS) $(SOURCE)/timed.c $(OBJECT)/*.o -o $(BUILD)/timed

main: mkdir c-all asm-all
	$(CC) $(CFLAGS) $(SOURCE)/main.c $(OBJECT)/*.o -o $(BUILD)/main

c-all:
	$(CC) $(CFLAGS) -c $(SOURCE)/image/image.c -o $(OBJECT)/image.o
	$(CC) $(CFLAGS) -c $(SOURCE)/bmp/bmp.c -o $(OBJECT)/bmp.o
	$(CC) $(CFLAGS) -c $(SOURCE)/file/file.c -o $(OBJECT)/file.o
	$(CC) $(CFLAGS) -c $(SOURCE)/trans/rot.c -o $(OBJECT)/rot.o
	$(CC) $(CFLAGS) -c $(SOURCE)/trans/sep.c -o $(OBJECT)/sep-c.o

asm-all:
	$(ASM) $(AFLAGS) $(SOURCE)/trans/sep.asm -o $(OBJECT)/sepia-asm.o

mkdir:
	mkdir -p $(BUILD) 
	mkdir -p $(OBJECT)

clean:
	rm -rf $(OBJECT)

clean-all: clean
	rm -rf $(BUILD) 
